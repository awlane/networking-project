from socket import *
from subprocess import call
from time import sleep, time
from threading import Thread, Event
from json import dumps, loads
from os.path import getsize

class ChatSession:

    def __init__(self, user, local_port, gui, save_dir):
        self.user = user
        self.main_socket = socket(AF_INET, SOCK_DGRAM)
        self.main_socket.bind(('',local_port))
        self.remote_IP = None
        self.remote_port = None
        self.save_dir = save_dir
        self.msg_log = open("{}/{}.log".format(self.save_dir, user), "a")
        self.debug_log = open("{}/{}debug.log".format(self.save_dir, user), "w+")
        self.ack = False
        self.stop = Event()
        self.sequence_num = 1
        self.ack_num = 0
        self.listener_thread = None
        self.queue_lock = None
        self.WINDOW = 2048
        self.CUMUL_ACK_TIME = 1.5
        self.TIMEOUT_TIME = 3
        self.CHUNK_SIZE = 1024
        self.send_queue = []
        self.msgs = dict()
        self.timeout_lock = Event()
        self.ack_lock = Event()
        self.ack_lock.set()
        self.msg_times = dict()
        self.connection = Event()
        self.stop_wait = Event()
        self.gui = gui

    def start_chat(self):
        # Start main loop
        self.timeout()
        self.listener_thread = Thread(name="main-{}".format(self.user), target=self.main_loop, daemon=True, args=())
        self.listener_thread.start()

    def wait_for_connection(self):
        # Initializes listener thread for handshake
        listener_thread = Thread(name="handshake-{}".format(self.user), target=self.wait_for_remote, daemon=True, args=())
        listener_thread.start()
        return listener_thread

    def wait_for_remote(self):
        # TCP Handshake as Server
        # Init timeout thread
        self.timeout()
        self.main_socket.settimeout(15.0)
        remote_found = False
        # We assume one connection per session
        while not self.connection.is_set() and not self.stop_wait.is_set():
            try:
                # Wait for SYN segment
                while not remote_found and not self.stop_wait.is_set():
                    # We only return the sender IP/port when it's necessary
                    return_val = self.recv_packet(True)
                    if return_val[6] == 3:
                        self.remote_IP, self.remote_port = return_val[0]
                        self.ack_num = return_val[4]
                        remote_found = True
                if self.stop_wait.is_set():
                    return
                # Respond with SYNACK segment
                self.transmit(msg_type = "SYNACK")
                ack = self.recv_packet()
                # Wait for final ack to start main loop
                if ack[5] == 2 and not self.stop_wait.is_set():
                    self.recv_ack(ack[4])
                    # self.debug_log.write("FINISHED HANDSHAKE")
                    # self.debug_log.flush()
                    self.connection.set()
                    self.main_socket.settimeout(None)
                    self.start_chat()
                    return
                else:
                    remote_found = False
                    if self.stop_wait.is_set():
                        return
            # If any timeouts occur, we assume an in-progress handshake did not complete
            # and clean up.
            except timeout:
                self.send_queue = []
                self.msgs = dict()
                self.msg_times = dict()
                remote_found = False
                if self.stop_wait.is_set():
                    return
                else:
                    continue

    def active_connection(self, IP_r, port):
        # TCP Handshake as Client
        # Init timeout thread
        self.timeout()
        try:
            # This prevents indefinite blocking
            self.main_socket.settimeout(15.0)
            success = False
            self.remote_IP = IP_r
            self.remote_port = port
            # Send the syn segment
            self.transmit(msg_type = "SYN")
            synack = self.recv_packet()
            # Receive the synack and transmit the ack
            if synack[5] == 4:
                self.ack_num = synack[3]
                self.recv_ack(synack[4])
                self.transmit(msg_type = "ACK")
                self.main_socket.settimeout(None)
                self.connection.set()
                success = True
            # Launch the main loop
            self.start_chat()
            return success
        except timeout:
            # Reset connection state to ensure that spurious retransmissions do not occur
            self.send_queue = []
            self.msgs = dict()
            self.msg_times = dict()
            raise timeout

    def main_loop(self):
        # stop is used to cleanly halt processing while allowing messages/file transfers to finish
        while not self.stop.is_set():
            message = bytearray()
            chunk, chunk_num, chunk_total, seq_number, ack_number, msg_type = self.recv_packet()
            # self.debug_log.write("{} {} {} {} {} {}\n".format(chunk, chunk_num, chunk_total, seq_number, ack_number, msg_type))
            # self.debug_log.flush()
            # We can wait for the next packet earlier if we don't need to do processing
            if msg_type == 2:
                self.recv_ack(ack_number)
                continue
            elif not seq_number == (self.ack_num + 1):
                self.send_ack()
                # self.debug_log.write("OOO {}\n".format(seq_number))
                # self.debug_log.flush()
                continue
            else:
                # self.debug_log.write("Ack {}\n".format(seq_number))
                # self.debug_log.flush()
                self.send_ack()
                self.ack_num = seq_number
            # self.debug_log.write("Received segment {}\n".format(seq_number))
            # self.debug_log.flush()
            # Handle newly acked segments
            self.recv_ack(ack_number)
            message.extend(chunk)
            # While there will not be chunked messages by default, it's still better to handle it
            while chunk_num < chunk_total:
                chunk, chunk_num, chunk_total, seq_numb, ack_numb, msg_type = self.recv_packet()
                if msg_type == 2:
                    self.recv_ack(ack_numb)
                    continue
                elif not seq_numb == (self.ack_num + 1):
                    self.send_ack()
                    continue
                else:
                    self.send_ack()
                    self.ack_num = seq_numb
                # self.debug_log.write("Received segment {}\n".format(seq_number))
                # self.debug_log.flush()
                self.recv_ack(ack_numb)
                message.extend(chunk)
            # Handle the compete message
            self.message_handler(message)
            # Ensure all messages have been written to disk
            self.msg_log.flush()
        # Close the msg_log file handle when the program exits
        self.msg_log.close()

    def recv_packet(self, ret_remote=False):
        # This receives segment data and parses the header values
        packet, remote = self.main_socket.recvfrom(2048)
        # self.debug_log.flush()
        header = packet[:12]
        seq_numb = int.from_bytes(header[:2], byteorder="big")
        ack_numb = int.from_bytes(header[2:4], byteorder="big")
        chunk_num = int.from_bytes(header[4:6], byteorder="big")
        chunk_total = int.from_bytes(header[6:8], byteorder="big")
        msg_size = int.from_bytes(header[8:10], byteorder="big")
        msg_type = int.from_bytes(header[10:12], byteorder="big")
        message = packet[12:(13+msg_size)]
        if ret_remote:
            return (remote, message, chunk_num, chunk_total, seq_numb, ack_numb, msg_type)
        return (message, chunk_num, chunk_total, seq_numb, ack_numb, msg_type)

    def message_handler(self, message):
        # This writes messages to the GUI and stores them in the history file
        msg_json = loads(message)
        self.gui.write_message(msg_json)
        msg = dumps(msg_json, ensure_ascii=False) + "\n"
        if msg_json["msg_type"] == "file":
            self.msg_log.write(msg)
            self.recv_file(msg_json)
        else:
            self.msg_log.write(msg)
            self.msg_log.flush()
        self.msg_log.flush()


    def recv_file(self, message):
        # This is the handler for file receipt, which is a separate method
        # to keep the main loop cleaner
        # self.debug_log.write("Receiving file...")
        # self.debug_log.flush()
        # Initial time for experiments
        test_time_A = time()
        file = open("{}/{}".format(self.save_dir, message["data"]), "wb+")
        chunk, chunk_num, chunk_total, seq_number, ack_number, msg_type = self.recv_packet()
        # Loops until the first expected data segment is received (allows us to init vars locally)
        while msg_type == 2 or not seq_number == (self.ack_num + 1):
            while msg_type == 2:
                self.recv_ack(ack_number)   
                # self.debug_log.write("{} Acked\n".format(seq_number))
                # self.debug_log.flush()  
                chunk, chunk_num, chunk_total, seq_number, ack_number, msg_type = self.recv_packet()
    
            while not seq_number == (self.ack_num + 1):
                self.send_ack()
                # self.debug_log.write("OOO {}\n".format(seq_number))
                # self.debug_log.flush()
                chunk, chunk_num, chunk_total, seq_number, ack_number, msg_type = self.recv_packet()

        # self.debug_log.write("Ack {}\n".format(seq_number))
        # self.debug_log.flush()
        self.send_ack()
        self.ack_num = seq_number
        file.write(chunk)
        # self.debug_log.write(str(chunk_total))
        # self.debug_log.flush()
        #Because this overrides the main loop, need to do other tasks
        self.recv_ack(ack_number)
        chunk_percent = 0
        while chunk_num < chunk_total:
            self.send_ack()
            chunk, chunk_num, chunk_total, seq_number, ack_number, msg_type = self.recv_packet()
            # Stop accepting chunks after total exceeded
            if (chunk_num >= chunk_total):
                # self.debug_log.write("c: {} t:{}\n".format(chunk_num, chunk_total))
                pass
            while msg_type == 2 or not seq_number == (self.ack_num + 1):
                while msg_type == 2:
                    self.recv_ack(ack_number)
                    # self.debug_log.write("Acked {}".format(seq_number))
                    # self.debug_log.flush()
                    self.send_ack()
                    chunk, chunk_num, chunk_total, seq_number, ack_number, msg_type = self.recv_packet()
                while not seq_number == (self.ack_num + 1):
                    self.send_ack()
                    # self.debug_log.write("OOO {}\n".format(seq_number))
                    # self.debug_log.flush()
                    chunk, chunk_num, chunk_total, seq_number, ack_number, msg_type = self.recv_packet()
                self.send_ack()

            # self.debug_log.write("Ack {}\n".format(seq_number))
            # self.debug_log.flush()
            self.send_ack()
            self.ack_num = seq_number
            # Write received chunk to file- this saves the receiver having 
            # to store it in memory
            file.write(chunk)
            new_chunk_percent = int((chunk_num / chunk_total) * 100)
            # Transmission percentage for receiver
            if not new_chunk_percent == chunk_percent:
                chunk_percent = new_chunk_percent
                if chunk_percent >= 100:
                    self.gui.write_to_text("{}%\n".format(str(chunk_percent)))
                else:
                    self.gui.write_to_text("{}%...".format(str(chunk_percent)))
                self.gui.root.update_idletasks()
            #self.recv_ack(ack_number)
        file.flush()
        test_time_B = time()
        total_time = test_time_B - test_time_A 
        # Testing data derived here
        self.debug_log.write("Seconds elapsed for file {}: {}".format(file.name, total_time))
        self.debug_log.flush()
        # This is done to automatically display images in the GUI
        image_types = ["jpg", "jpeg", "png", "gif"]
        if any(ext in file.name for ext in image_types):
            self.gui.display_image(file.name)
        file.close()
        return

    def send_msg(self, message, msg_type="msg"):
        # Used for sending application level messages
        msg = {"sender":self.user,"time":int(time()),"msg_type":msg_type,"data":message}
        msg_json = dumps(msg)
        self.gui.write_message(msg)
        temp = dumps(msg, ensure_ascii=False) + "\n"
        self.msg_log.write(temp)
        self.msg_log.flush()
        message_bytes = bytes(msg_json, "utf-8")
        message_size = len(message_bytes)
        self.transmit(message = message_bytes, message_size = message_size)
        return

    def send_file(self, path):
        #Determine file size and number of chunks
        file_size = getsize(path)
        chunk_num = 1
        chunk_total = 0
        if file_size % self.CHUNK_SIZE >= 1:
            chunk_total = (file_size // self.CHUNK_SIZE) + 1
        else:
            chunk_total = (file_size / self.CHUNK_SIZE)
        #Indicate to receiver desire to send file
        self.send_msg(path.split("/")[-1], "file")
        #Begin reading and transmitting file
        iterate_file = open(path, mode="rb")
        chunk = bytearray()
        chunk_bytes = iterate_file.read(self.CHUNK_SIZE)
        chunk.extend(chunk_bytes)
        chunk_percent = 0
        while True:
            #Loop until the file is fully read
            if chunk_bytes:
                #Alternates between reading chunks and transmitting them, prevents sending empty segments
                if len(chunk) == 0:
                    chunk_bytes = iterate_file.read(self.CHUNK_SIZE)
                    chunk.extend(chunk_bytes)
                else:
                    # We transmit the chunk and display the progress percentage on the GUI
                    self.transmit(message = chunk, message_size = self.CHUNK_SIZE, chunk_num=chunk_num, chunk_total=chunk_total)
                    chunk_num += 1
                    new_chunk_percent = int((float(chunk_num) / float(chunk_total)) * 100)
                    if not new_chunk_percent == chunk_percent:
                        chunk_percent = new_chunk_percent
                        if chunk_percent >= 100:
                            self.gui.write_to_text("{}%\n".format(str(chunk_percent)))
                        else:
                            self.gui.write_to_text("{}%...".format(str(chunk_percent)))
                    self.gui.root.update_idletasks()
                    packet = bytearray()
                    chunk = bytearray()
            else:
                break

        iterate_file.close()

    def gen_header(self, sequence_num, ack_num, chunk_num, total_chunks, message_size, msg_type):
        #Generates the binary header from the given parameters
        packet_type = {"SEG":1,"ACK":2,"SYN":3,"SYNACK":4}
        header = bytearray()
        seq = sequence_num.to_bytes(2,byteorder="big")
        header.extend(seq)
        ack = ack_num.to_bytes(2,byteorder="big")
        header.extend(ack)
        chunk = chunk_num.to_bytes(2,byteorder="big")
        header.extend(chunk)
        total = total_chunks.to_bytes(2,byteorder="big")
        header.extend(total)
        size = message_size.to_bytes(2,byteorder="big")
        header.extend(size)
        pkt_type = packet_type[msg_type].to_bytes(2,byteorder="big")
        header.extend(pkt_type)
        return header

    def transmit(self, message = None, message_size = 0, chunk_num = 1, chunk_total = 1, msg_type = "SEG"):
        #Block sending new messages until sending window is below max length
        while len(self.send_queue) >= self.WINDOW:
            pass
        packet = None
        # self.debug_log.write("Sending ({},{})\n".format(self.sequence_num, self.ack_num))
        # self.debug_log.flush()
        #Each segment type is handled slightly differently, ergo the code duplication
        if msg_type == "SEG":
            seq_number = self.sequence_num
            self.send_queue.append(self.sequence_num)
            packet = self.gen_header(self.sequence_num, self.ack_num, chunk_num, chunk_total, message_size, msg_type)
            self.sequence_num += 1
            packet.extend(message)
            self.msgs[seq_number] = packet
            self.msg_times[seq_number] = time()
        elif msg_type == "ACK":
            packet = self.gen_header(self.sequence_num, self.ack_num, chunk_num, chunk_total, message_size, msg_type)
        elif msg_type == "SYN" or msg_type == "SYNACK":
            seq_number = self.sequence_num
            self.send_queue.append(self.sequence_num)
            packet = self.gen_header(self.sequence_num, self.ack_num, chunk_num, chunk_total, message_size, msg_type)
            self.sequence_num += 1
            self.msgs[seq_number] = packet
            self.msg_times[seq_number] = time()

        self.main_socket.sendto(packet, (self.remote_IP, self.remote_port))

    def send_ack(self):
        #If the ack period has not begun, start it
        if not self.ack:
            self.ack = True
            Thread(name="ack-{}".format(self.user), target=self.cumul_ack, daemon=True).start()
        #Else, do nothing
        else:
            return
    
    def timeout(self):
        # self.debug_log.write("Starting timeout")
        # self.debug_log.flush()
        #If the timeout period has not begun, start it
        if not self.timeout_lock.is_set():
            Thread(name="timeout-{}".format(self.user), target=self.retransmit, daemon=True).start()
        #Else, do nothing
        else:
            return

    def retransmit(self):
        #Locks are used so it does not retransmit while segments are being acked.
        try:
            self.timeout_lock.set()
            #Slightly hacky but means that timer is effectively restarted on oldest
            sleep(self.TIMEOUT_TIME)
            # self.debug_log.write("Waiting on ack")
            # self.debug_log.flush()
            self.ack_lock.wait()
            self.ack_lock.clear()
            retrans_time = time()
            temp_queue = self.send_queue[:]
            unacked = False
            #Check if oldest packet has exceeded timeout range
            if len(temp_queue) > 0 and (retrans_time - self.msg_times[temp_queue[0]]) >= self.TIMEOUT_TIME:
                for msg in temp_queue:
                    self.main_socket.sendto(self.msgs[msg], (self.remote_IP, self.remote_port))
            # self.debug_log.write("Retransed")
            # self.debug_log.flush()
            self.ack_lock.set()
            self.timeout_lock.clear()
            self.timeout()
        except Exception as e:
            self.debug_log.write(str(e))
            self.debug_log.flush() 

    def cumul_ack(self):
        #Wait 3 seconds and transmit an ack for all of the packets
        sleep(3)
        self.transmit(msg_type = "ACK")
        self.ack = False
        #self.send_ack()
    
    def recv_ack(self, number):
        #Avoid concurrency issues by copying the queue as it is at time of ack
        # self.debug_log.write("Waiting on lock")
        # self.debug_log.flush()
        self.ack_lock.wait()
        self.ack_lock.clear()
        # self.debug_log.write("Got lock")
        # self.debug_log.flush()
        temp_queue = self.send_queue[:]
        for msg in temp_queue:
            #Remove acked segments from all data structures
            if number >= msg:
                self.send_queue.remove(msg)
                if msg in self.msgs:
                    del self.msgs[msg]
                if msg in self.msg_times:
                    del self.msg_times[msg]
        self.ack_lock.set()