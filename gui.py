from tkinter import *
from tkinter import filedialog
from tkinter import messagebox, scrolledtext
from subprocess import call
from client import ChatSession
from gui_helper import Dialog
from sys import argv
from ipaddress import ip_address
from datetime import datetime
from socket import timeout
from json import loads
from PIL import Image, ImageTk
from os import mkdir

#TODO: Fix retransmit on pairing?
#\U0001f600 works- byte padding nonsense!!!!
#Randomized seq num
#Add exit code
#Add sequence number rollover

class gui:
    def __init__(self, user, port):
        self.root = Tk()
        menu = Menu(self.root)
        self.root.config(menu=menu)
        self.root.title("AliasChat ({})".format(user))
        filemenu = Menu(menu)
        menu.add_cascade(label="File", menu=filemenu)
        filemenu.add_command(label="Connect...", command=self.connect)
        filemenu.add_command(label="Wait for Connection...", command=self.wait_connect)
        filemenu.add_separator()
        filemenu.add_command(label="Exit", command=self.kill)
        helpmenu = Menu(menu)
        menu.add_cascade(label="Help", menu=helpmenu)
        helpmenu.add_command(label="About", command=self.about)

        self.text = scrolledtext.ScrolledText(self.root)
        self.text.grid(row=0, column=0, sticky="NSEW")
        self.e = Entry(self.root)
        self.e.grid(row=1, column=0, sticky="NSEW")
        self.b1 = Button(self.root, text="Send", width=10, command=self.send_message)
        self.b1.grid(row=1, column=1)
        self.b2 = Button(self.root, text="Send File", width=10, command=self.send_file)
        self.b2.grid(row=1, column=2)
        self.root.bind("<Return>", self.send_message)

        self.text.tag_config("file", foreground="blue")
        #self.text.tag_bind("file", "<Button-1>", self.open_file)
        self.text.config(state=DISABLED)
        self.user = user
        # This can be changed arbitrarily but is the cleanest way to handle it for experiments
        self.save_dir = "/tmp/CHAT{}".format(self.user)
        try:
            mkdir(self.save_dir)
        except FileExistsError:
            pass
        #Hack to make images work- Tkinter can't keep image refences in tree like it can widgets
        self.image_dict = dict()
        self.file_path_dict = dict()
        self.log_recover(self.user)

        self.chat = ChatSession(self.user, int(port), self, self.save_dir)
        self.root.protocol("WM_DELETE_WINDOW", self.kill)
        
        self.root.mainloop()

    def send_file(self):
        # Transmit and display files
        file = filedialog.askopenfilename()
        if not file:
            return
        self.write_to_text("[INFO]: Sending file...\n")
        self.root.update_idletasks()
        self.chat.send_file(file)
        file_name = file.split("/")[-1]
        self.file_path_dict[file_name] = file
        image_types = ["jpg", "jpeg", "png", "gif"]
        if any(ext in file for ext in image_types):
            self.display_image(file)
        self.root.update_idletasks()
        self.write_to_text("[INFO]: File sent!\n")

    def display_image(self, file):
        # Helper method to display images, prevents crashes if files move
        try:
            base_image = Image.open(file)
            image = ImageTk.PhotoImage(base_image)
        except IOError:
            print("Could not open file")
            return
        self.image_dict[file] = image
        self.text.image_create(END, image=self.image_dict[file])
        self.write_to_text("\n")

    def write_message(self, json):
        # Formats messages for GUI
        time = datetime.fromtimestamp(json["time"]).strftime("%m/%d/%y %H:%M:%S")
        tags = None
        if json["msg_type"] == "msg":
            out_msg = "{} [{}]: {}\n".format(time, json["sender"], json["data"])
        elif json["msg_type"] == "file":
            out_msg = "{} * {} sent a file: ".format(time, json["sender"])
            if json["sender"] != self.user:
                self.file_path_dict[json["data"]] = "{}/{}".format(self.save_dir, json["data"])
            self.write_to_text(out_msg)
            out_msg = "{}\n".format(json["data"])
            tags = "file"
        self.write_to_text(out_msg, tags)

    def write_to_text(self, msg, flag=None):
        # This prevents arbitrary text insertion by user into text fields
        self.text.config(state=NORMAL)
        if not flag:
            self.text.insert(END, msg)
        else:
            self.text.insert(END, msg, flag)
        self.text.config(state=DISABLED)

    def send_message(self, event=None):
        # Handler for sending message
        message = str(self.e.get())
        if message == "":
            return
        if len(message) > 512:
            messagebox.showerror("Error", "Messages cannot be longer than 512 characters!")
            return
        if not self.chat.connection.is_set():
            messagebox.showerror("Error", "Not connected!")
            return
        self.chat.send_msg(message)
        self.e.delete(0,END)

    #NOTE: Only works on Linux and is inconsistent due to running as superuser
    # def open_file(self, event):
    #     xy = "@{},{}".format(event.x, event.y)
    #     tag_range = self.text.tag_prevrange("file", xy)
    #     file = self.text.get(*tag_range).strip("\n")
    #     try:
    #         file_path = self.file_path_dict[file]
    #     except KeyError:
    #         messagebox.showerror(title="Error", message="File not found")
    #     # Make dict to make this work?
    #     call(["xdg-open", file_path])

    def about(self):
        messagebox.showinfo(title="About", message="AliasChat v1.0.0\ncreated by Alex Lane for COMP-3825")

    # def callback(self, event=None):
    #     self.write_to_text("NOT IMPLEMENTED")
    #     return

    def connect(self):
        # Handles the connection menu for client mode
        dialog = ConnectDialog(self.root, "Connect To...")
        print(dialog.result)
        connected = False
        try:
            connected = self.chat.active_connection(dialog.result[0], dialog.result[1])
        except timeout:
            #messagebox.showerror(title="Error", message="Failed to connect.")
            pass
        if not connected:
            messagebox.showerror(title="Error", message="Failed to connect.")
        else:
            messagebox.showinfo(title="Success", message="Connected.")

    def wait_connect(self):
        # Handles the connection menu for server mode
        self.chat.stop_wait.clear()
        listening_thread = self.chat.wait_for_connection()
        connecting = WaitForConnectDialog(self.root, self.chat, listening_thread)
        
    def log_recover(self, user):
        # This retrieves and outputs message logs to GUI
        try:
            file = open("{}/{}.log".format(self.save_dir, user), "r")
        except FileNotFoundError:
            return
        messages = []
        for line in file.readlines():
            messages.append(loads(line))
        ordered_msg = sorted(messages, key=lambda message: message["time"])
        for json in ordered_msg:
            self.write_message(json)
            if json["msg_type"] == "file":
                image_types = ["jpg", "jpeg", "png", "gif"]
                if any(ext in json["data"] for ext in image_types):
                    self.display_image("{}/{}".format(self.save_dir, json["data"]))
        file.close()
        self.root.update_idletasks()

    
    def kill(self):
        #Stop GUI and client
        self.chat.stop.set()
        self.root.destroy()

class WaitForConnectDialog(Toplevel):
    # Helper class for server mode connection
    def __init__(self, parent, chat, thread):
        Toplevel.__init__(self, parent)
        self.thread = thread
        self.chat = chat
        self.transient(parent)
        self.title("Waiting for connect...")
        self.parent = parent
        body = Frame(self)
        self.l1 = Label(body, text="Waiting for connection...")
        self.l1.grid(row=0)
        body.pack(padx=5, pady=5)
        box = Frame(self)
        self.b1 = Button(box, text="OK", width=10, command=self.ok, default=ACTIVE, state=DISABLED)
        self.b1.pack(side=LEFT, padx=5, pady=5)
        self.b2 = Button(box, text="Cancel", width=10, command=self.cancel)
        self.b2.pack(side=LEFT, padx=5, pady=5)
        self.bind("<Return>", self.ok)
        self.bind("<Escape>", self.cancel)
        box.pack()
        self.grab_set()
        self.protocol("WM_DELETE_WINDOW", self.cancel)
        self.geometry("+{}+{}".format(parent.winfo_rootx()+50, parent.winfo_rooty()+5))
        self.focus_set()
        self.after(100, self.check_running)
        self.wait_window(self)

    def check_running(self):
        # Once connected, thread exits
        if self.thread.is_alive():
            #after, check thread
            self.after(100, self.check_running)
        # Enable user to exit connection menu
        else:
            self.l1.config(text="Connected!")
            self.b1.config(state=NORMAL)

    def ok(self):
        self.withdraw()
        self.update_idletasks()
        self.parent.focus_set()
        self.destroy()

    def cancel(self):
        self.chat.stop_wait.set()
        self.withdraw()
        self.update_idletasks()
        self.parent.focus_set()
        self.destroy()

class ConnectDialog(Dialog):
    # Used to enter IP address and port # for client mode
    def body(self, master):
        Label(master, text="IP:").grid(row=0)
        Label(master, text="Port:").grid(row=1)
        self.e1 = Entry(master)
        self.e2 = Entry(master)
        self.e1.grid(row=0, column=1)
        self.e2.grid(row=1, column=1)
        return self.e1
    def apply(self):
        ip = str(self.e1.get())
        port = int(self.e2.get())
        self.result = (ip, port)
    def validate(self):
        if self.e1.get() and self.e2.get():
            try:
                if isinstance(int(self.e2.get()), int):
                    try:
                        temp = ip_address(self.e1.get())
                        return True
                    except ValueError:
                        if self.e1.get() != "localhost":
                            messagebox.showerror(title="Error", message="Please enter a valid port and IP number")
                            return False
                        return True
            except ValueError:
                messagebox.showerror(title="Error", message="Please enter a valid port and IP number")
                return False
        else:
            messagebox.showerror(title="Error", message="Please enter a valid port and IP number")
            return False

if __name__ == '__main__':
    g = gui(sys.argv[1], sys.argv[2])