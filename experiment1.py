from mininet.topo import Topo
from mininet.net import Mininet
from mininet.node import Host
from mininet.link import TCLink
from mininet.log import setLogLevel, info
from mininet.cli import CLI
from time import sleep

debug_a = open("/tmp/adebug", "w")
debug_b = open("/tmp/bdebug", "w")
net = Mininet()
host_A = net.addHost("h1", ip="10.0.0.1")
host_B = net.addHost("h2", ip="10.0.0.2")
switch = net.addSwitch("s1")
link = net.addLink(host_A, host_B, cls=TCLink)
net.start()
a = host_A.popen("python3 gui.py a 5309", stdout=debug_a, stderr=debug_a)
b = host_B.popen("python3 gui.py b 5309", stdout=debug_b, stderr=debug_b)
sleep(5)
# link.intf1.config(bw=10.0, delay='5ms')
# link.intf2.config(bw=10.0, delay='5ms')
link.intf1.config(loss=10, bw=10.0, delay="5ms")
link.intf2.config(loss=10,  bw=10.0, delay="5ms")
# link.intf1.config(loss=5, bw=10.0, delay="5ms")
# link.intf2.config(loss=5,  bw=10.0, delay="5ms")
CLI(net)
net.stop()